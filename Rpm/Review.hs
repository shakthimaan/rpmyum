{-
 -
 - Copyright 2012 Shakthi Kannan 
 -
 - This program is free software; you can redistribute it and/or modify it
 - under the terms of the GNU General Public License as published by the
 - Free Software Foundation; either version 2 of the license, or (at your
 - option) any later version.  See http://www.gnu.org/copyleft/gpl.html for
 - the full text of the license.
 -
 - E-mail: shakthimaan at fedoraproject dot org
 - Web:    shakthimaan.com
 -}

import System.Process
import System.Exit
import System.Directory

type Bug = String

fedoraReview :: String
fedoraReview = "fedora-review -b "

reviewDir :: String
reviewDir = "/tmp"

runReview :: Bug -> IO ()
runReview b = do
  setCurrentDirectory reviewDir
  let fullCmd = fedoraReview ++ b
  ex <- system fullCmd
  case ex of
    ExitFailure eno -> putStrLn ("\nERROR: " ++ fullCmd ++ "\n")
    ExitSuccess     -> putStrLn ("\nSUCCESS: " ++ fullCmd ++ "\n")

