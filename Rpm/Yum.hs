{-# LANGUAGE DeriveDataTypeable #-}

{-
 -
 - Copyright 2012 Shakthi Kannan 
 -
 - This program is free software; you can redistribute it and/or modify it
 - under the terms of the GNU General Public License as published by the
 - Free Software Foundation; either version 2 of the license, or (at your
 - option) any later version.  See http://www.gnu.org/copyleft/gpl.html for
 - the full text of the license.
 -
 - E-mail: shakthimaan at fedoraproject dot org
 - Web:    shakthimaan.com
 -}

import System.Process
import System.Exit
import Text.Regex.Posix
import System.Path
import System.IO.HVFS
import System.IO.Temp
import Data.List
import Data.Maybe
import System.Directory
import Control.Concurrent
import Control.Parallel
import System.Console.CmdArgs
import Test.QuickCheck

type Spec = String

data Yum = Yum 
     {input :: Spec
     ,prepare :: Bool
     ,compile :: Bool
     ,build :: Bool
     ,source :: Bool
     ,allb :: Bool
     ,lint :: Bool
     ,koji :: Bool
     ,everything :: Bool
     }
     deriving (Show, Data, Typeable)

s = Yum{input = def &= help "Name of .spec file"
  ,prepare = def &= help "rpmbuild -bp"
  ,compile = def &= help "rpmbuild -bc"
  ,build = def &= help "rpmbuild -bb"
  ,source = def &= help "rpmbuild -bs"
  ,allb = def &= help "rpmbuild -ba"
  ,lint = def &= help "rpmlint"
  ,koji = def &= help "koji builds"
  ,everything = def &= help "rpmbuild, rpmlint, koji"}

homePath :: String -> String
homePath = (++) "/home/"

prop_homepath_first6 xs = take 6 (homePath xs) == "/home/" 

fullSpec :: String -> String -> String
fullSpec user spec = foldl (++) "" ["/home/", user, "/rpmbuild/SPECS/", spec]

prop_fullspec_first6 xs ys = take 6 (fullSpec xs ys) == "/home/"

prop_fullspec_path xs ys = take 16 (drop (6 + length xs) (fullSpec xs ys)) == "/rpmbuild/SPECS/"

rpmbuildPath :: String -> String
rpmbuildPath user = foldl (++) "" ["/home/", user, "/rpmbuild"]

prop_rpmbuildPath_first6 xs = take 6 (rpmbuildPath xs) == "/home/"

prop_rpmbuildPath_dir xs = take 9 (drop (6 + length xs) (rpmbuildPath xs)) == "/rpmbuild"

kojiURL :: String
kojiURL = " http://koji.fedoraproject.org/koji/taskinfo?taskID="

rpmbuildCmd :: String -> String -> String
rpmbuildCmd command full_spec = foldl (++) "" ["rpmbuild ", command, " ", full_spec]

prop_rpmbuildcmd_first9 xs ys = take 9 (rpmbuildCmd xs ys) == "rpmbuild "

getUsername :: IO [Char]
getUsername = do
             text <- readProcess "whoami" [] []
             return $ filter (/= '\n') text

runRpmBuild :: Spec -> String -> IO Bool
runRpmBuild spec command = do
             username <- getUsername
             let x = fullSpec username spec
             let fullCmd = rpmbuildCmd command x
             ex <- system fullCmd
             case ex of
                  ExitFailure eno -> putStrLn ("\nERROR: " ++ fullCmd ++ "\n") >> return False
                  ExitSuccess     -> putStrLn ("\nSUCCESS: " ++ fullCmd ++ "\n") >> return True

runRpmLint :: String -> IO ()
runRpmLint filename = do
           let fullCmd = "rpmlint " ++ filename
           putStrLn fullCmd
           ex <- system fullCmd
           case ex of
                ExitFailure eno -> print ex
                ExitSuccess -> putStrLn ""

getFilePaths :: FilePath -> String -> IO FilePath
getFilePaths path filename = do
            a <- recurseDir SystemFS path
            let b = find (=~ filename) a
            case b of
                 Just x -> do
                      c <- doesFileExist x
                      return x
                 Nothing -> return ""

getSpecRpmbuildPath :: String -> IO (String, String)
getSpecRpmbuildPath spec = do
             username <- getUsername 
             let x = fullSpec username spec
             let y = rpmbuildPath username
             return (x, y)                  

runRpmLintSpec :: String -> IO ()
runRpmLintSpec spec = do
             username <- getUsername 
             let x = fullSpec username spec
             runRpmLint x

runRpmLintDevel :: String -> IO ()
runRpmLintDevel spec = do
             (x, y) <- getSpecRpmbuildPath spec
             (ex, sout, serr) <- readProcessWithExitCode "rpm" ["-q", "--specfile", "--qf", "[%{name}-%{version}-%{release}.%{arch}.rpm\n]", x] []
             let a = words sout
             print a
             b <- mapM (getFilePaths y) a
             mapM_ runRpmLint b

runRpmLintSRPM :: String -> IO ()
runRpmLintSRPM spec = do
             (x, y) <- getSpecRpmbuildPath spec
             (ex, sout, serr) <- readProcessWithExitCode "rpm" ["-q", "--specfile", "--qf", "[%{name}-%{version}-%{release}.src.rpm\n]", x] []
             let b = words sout
             c <- getFilePaths y $ head b
             runRpmLint c

runRpmLintAll :: Spec -> IO Bool
runRpmLintAll spec = do
              mapM_ ($ spec) [runRpmLintSpec, runRpmLintDevel, runRpmLintSRPM]
              return True

getSRPMName spec = do
             (x, y) <- getSpecRpmbuildPath spec
             (ex, sout, serr) <- readProcessWithExitCode "rpm" ["-q", "--specfile", "--qf", "[%{name}-%{version}-%{release}.src.rpm\n]", x] []
             let b = words sout
             c <- getFilePaths y $ head b
             return c

getKojiStateInfo :: String -> IO Bool
getKojiStateInfo taskid = do
                 text <- readProcess "koji" ["taskinfo", taskid] []
                 let state_match = [ x | x <- (lines text), x =~ "State: [a-z]+" ]
                 let status = (head state_match) =~ "[a-z]+$"
                 case status of "closed" -> return True
                                "failed" -> return False
                                otherwise -> do
                                                threadDelay (30 * 1000000)
                                                getKojiStateInfo taskid

runTagKoji :: String -> String -> IO ()
runTagKoji srpm tag = do
           text <- readProcess "koji" ["build", "--scratch", "--nowait", tag, srpm] []
           let created_match = [ x | x <- (lines text), x =~ "Created task: [0-9]+" ]
           let taskid = (head created_match) =~ "[0-9]+"
           putStrLn ("Koji " ++ tag ++ kojiURL ++ taskid)
           a <- getKojiStateInfo taskid
           if a
              then putStrLn ("Koji Success " ++ tag ++ kojiURL ++ taskid)
              else putStrLn ("Koji Failure " ++ tag ++ kojiURL ++ taskid)

parallelKoji :: Spec -> IO Bool
parallelKoji spec = do
	srpm <- getSRPMName spec
        str <- newEmptyMVar
        mvar <- newEmptyMVar
        let work val = runTagKoji srpm val >> putMVar mvar ()
        mapM_ (forkIO . work) values >> return True
        mapM_ (\_ -> takeMVar mvar) values >> return True
              where values = ["f17", "f18", "f19", "f20"]

runRpmBuildAll a = do
               x <- runRpmBuild (input a) "-ba"
               if x
                  then do
                       y <- runRpmLintAll (input a)
                       if y
                          then parallelKoji (input a)
                          else return False
                  else return False

dispatch :: Yum -> IO Bool
dispatch a
         | prepare a = runB "-bp"
         | compile a = runB "-bc"
         | build a = runB "-bb"
         | source a = runB "-bs"
         | allb a = runB "-ba"
         | lint a = runRpmLintAll (input a)
         | koji a = parallelKoji (input a)
         | everything a = runRpmBuildAll a
         | otherwise = putStrLn "Invalid option" >> return False
         where
                runB = runRpmBuild (input a)

main = do
     a <- cmdArgs s
     dispatch a
